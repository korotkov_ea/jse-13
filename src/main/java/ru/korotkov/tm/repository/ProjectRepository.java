package ru.korotkov.tm.repository;

import ru.korotkov.tm.entity.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    /**
     * Create project by name
     *
     * @param name - name of project
     * @param userId
     * @return created project
     */
    public Project create(final String name, final Long userId) {
        final Project project = new Project(name, userId);
        projects.add(project);
        return project;
    }

    /**
     * Create project by name and description
     *
     * @param name        - name of project
     * @param description - description of project
     * @param userId
     * @return created project
     */
    public Project create(final String name, final String description, final Long userId) {
        final Project project = new Project(name, description, userId);
        projects.add(project);
        return project;
    }

    /**
     * Clear projects
     *
     * @param userId
     */
    public void clear(final Long userId) {
        projects.removeAll(findAll(userId));
    }

    /**
     * Get size of repository
     *
     * @param userId
     */
    public int getSize(final Long userId) {
        return findAll(userId).size();
    }

    /**
     * Find project by index
     *
     * @param index index of project
     * @param userId
     * @return project or null
     */
    public Project findByIndex(final int index, final Long userId) {
        Project project = findAll(userId).get(index);
        if (project == null) {
            return null;
        }
        if (!project.getUserId().equals(userId)) {
            return null;
        }
        return project;
    }

    /**
     * Find project by name
     *
     * @param name name of project
     * @param userId
     * @return project or null
     */
    public Project findByName(final String name, final Long userId) {
        Project result = null;
        for (final Project project : projects) {
            if (name.equals(project.getName())) {
                result = project;
                break;
            }
        }
        if (result != null && !result.getUserId().equals(userId)) {
            return null;
        }
        return result;
    }

    /**
     * Find project by id
     *
     * @param id id of project
     * @param userId
     * @return project or null
     */
    public Project findById(final Long id, final Long userId) {
        Project result = null;
        for (final Project project : projects) {
            if (id.equals(project.getId())) {
                result = project;
                break;
            }
        }
        if (result != null && !result.getUserId().equals(userId)) {
            return null;
        }
        return result;
    }

    /**
     * Remove project by index
     *
     * @param index index of project
     * @param userId
     * @return project or null
     */
    public Project removeByIndex(final int index, final Long userId) {
        Project project = findByIndex(index, userId);
        if (project != null) {
            projects.remove(project);
        }
        return project;
    }

    /**
     * Remove project by name
     *
     * @param name name of project
     * @param userId
     * @return project or null
     */
    public Project removeByName(final String name, final Long userId) {
        Project project = findByName(name, userId);
        if (project != null) {
            projects.remove(project);
        }
        return project;
    }

    /**
     * Remove project by id
     *
     * @param id id of project
     * @param userId
     * @return project or null
     */
    public Project removeById(final Long id, final Long userId) {
        Project project = findById(id, userId);
        if (project != null) {
            projects.remove(project);
        }
        return project;
    }

    /**
     * Update project by index
     *
     * @param index index of project
     * @param name name of project
     * @param userId
     * @return project or null
     */
    public Project updateByIndex(final int index, final String name, final Long userId) {
        Project project = findByIndex(index, userId);
        if (project == null) {
            return null;
        }
        project.setName(name);
        return project;
    }

    /**
     * Update project by index
     *
     * @param index index of project
     * @param name name of project
     * @param description description of project
     * @param userId
     * @return project or null
     */
    public Project updateByIndex(final int index, final String name, final String description, final Long userId) {
        Project project = findByIndex(index, userId);
        if (project == null) {
            return null;
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    /**
     * Update project by id
     *
     * @param id id of project
     * @param name name of project
     * @param userId
     * @return project or null
     */
    public Project updateById(final Long id, final String name, final Long userId) {
        Project project = findById(id, userId);
        if (project == null) {
            return null;
        }
        project.setName(name);
        return project;
    }

    /**
     * Update project by id
     *
     * @param id id of project
     * @param name name of project
     * @param description description of project
     * @param userId
     * @return project or null
     */
    public Project updateById(final Long id, final String name, final String description, final Long userId) {
        Project project = findById(id, userId);
        if (project == null) {
            return null;
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    /**
     * Return projects
     *
     * @param userId
     */
    public List<Project> findAll(final Long userId) {
        ArrayList<Project> result = new ArrayList<Project>();
        for (final Project project: projects) {
            if (project.getUserId().equals(userId)) {
                result.add(project);
            }
        }
        return result;
    }

}