package ru.korotkov.tm.repository;

import ru.korotkov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    /**
     * Create task by name
     *
     * @param name - name of task
     * @param userId
     * @return created task
     */
    public Task create(final String name, final Long userId) {
        final Task task = new Task(name, userId);
        tasks.add(task);
        return task;
    }

    /**
     * Create task by name and description
     *
     * @param name        - name of task
     * @param description - description of task
     * @param userId
     * @return created task
     */
    public Task create(final String name, final String description, final Long userId) {
        final Task task = new Task(name, description, userId);
        tasks.add(task);
        return task;
    }

    /**
     * Clear tasks
     *
     * @param userId
     */
    public void clear(final Long userId) {
        tasks.removeAll(findAll(userId));
    }

    /**
     * Get size of repository
     *
     * @param userId
     */
    public int getSize(final Long userId) {
        return findAll(userId).size();
    }

    /**
     * Find task by index
     *
     * @param index index of task
     * @param userId
     * @return task or null
     */
    public Task findByIndex(final int index, final Long userId) {
        Task task = findAll(userId).get(index);
        if (task == null) {
            return null;
        }
        if (!task.getUserId().equals(userId)) {
            return null;
        }
        return task;
    }

    /**
     * Find task by name
     *
     * @param name name of task
     * @param userId
     * @return task or null
     */
    public Task findByName(final String name, final Long userId) {
        Task result = null;
        for (final Task task : tasks) {
            if (name.equals(task.getName())) {
                result = task;
                break;
            }
        }
        if (result != null && !result.getUserId().equals(userId)) {
            return null;
        }
        return result;
    }

    /**
     * Find task by id
     *
     * @param id id of task
     * @param userId
     * @return task or null
     */
    public Task findById(final Long id, final Long userId) {
        Task result = null;
        for (final Task task : tasks) {
            if (id.equals(task.getId())) {
                result = task;
                break;
            }
        }
        if (result != null && !result.getUserId().equals(userId)) {
            return null;
        }
        return result;
    }

    /**
     * Remove task by index
     *
     * @param index index of task
     * @param userId
     * @return task or null
     */
    public Task removeByIndex(final int index, final Long userId) {
        Task task = findByIndex(index, userId);
        if (task != null) {
            tasks.remove(task);
        }
        return task;
    }

    /**
     * Remove task by name
     *
     * @param name name of task
     * @param userId
     * @return task or null
     */
    public Task removeByName(final String name, final Long userId) {
        Task task = findByName(name, userId);
        if (task != null) {
            tasks.remove(task);
        }
        return task;
    }

    /**
     * Remove task by id
     *
     * @param id id of task
     * @param userId
     * @return task or null
     */
    public Task removeById(final Long id, final Long userId) {
        Task task = findById(id, userId);
        if (task != null) {
            tasks.remove(task);
        }
        return task;
    }

    /**
     * Update task by index
     *
     * @param index index of task
     * @param name name of task
     * @param userId
     * @return task or null
     */
    public Task updateByIndex(final int index, final String name, final Long userId) {
        Task task = findByIndex(index, userId);
        if (task == null) {
            return null;
        }
        task.setName(name);
        return task;
    }

    /**
     * Update task by index
     *
     * @param index index of task
     * @param name name of task
     * @param description description of task
     * @param userId
     * @return task or null
     */
    public Task updateByIndex(final int index, final String name, final String description, final Long userId) {
        Task task = findByIndex(index, userId);
        if (task == null) {
            return null;
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    /**
     * Update task by id
     *
     * @param id id of project
     * @param name name of project
     * @param userId
     * @return project or null
     */
    public Task updateById(final Long id, final String name, final Long userId) {
        Task task = findById(id, userId);
        if (task == null) {
            return null;
        }
        task.setName(name);
        return task;
    }

    /**
     * Update task by id
     *
     * @param id id of project
     * @param name name of project
     * @param description description of project
     * @param userId
     * @return project or null
     */
    public Task updateById(final Long id, final String name, final String description, final Long userId) {
        Task task = findById(id, userId);
        if (task == null) {
            return null;
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    /**
     * Return tasks
     *
     * @param userId
     */
    public List<Task> findAll(final Long userId) {
        ArrayList<Task> result = new ArrayList<>();
        for (final Task task: tasks) {
            if (task.getUserId().equals(userId)) {
                result.add(task);
            }
        }
        return result;
    }

    /**
     * Return tasks by projectId
     *
     * @param projectId id of project
     * @param userId
     */
    public List<Task> findTaskByProjectId(final Long projectId, final Long userId) {
        List<Task> result = new ArrayList<>();
        for (final Task task: findAll(userId)) {
            if (task.getProjectId().equals(projectId)) {
                result.add(task);
            }
        }
        return result;
    }

}