package ru.korotkov.tm.enumerated;

public enum Role {

    USER("User"),

    ADMIN("Admin");

    private final String name;

    Role(final String name) {
        this.name = name;
    }

    public String displayName() {
        return name;
    }

    @Override
    public String toString() {
        return displayName();
    }

}
